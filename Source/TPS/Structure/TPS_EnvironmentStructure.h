// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS_EnvironmentStructure.generated.h"

UCLASS()
class TPS_API ATPS_EnvironmentStructure : public AActor, public ITPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPS_EnvironmentStructure();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USphereComponent* CollisionSphere = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExploseSetting")
		FProjectileInfo ExploseSetting;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Interface
	UFUNCTION(BlueprintCallable)
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
	void AddEffect(UTPS_StateEffect* newEffect) override;
	UFUNCTION(BlueprintCallable)
	FVector GetStateEffectLocBone(FName& BoneName) override;

	//Explose
	UFUNCTION(BlueprintCallable)
	virtual void Explose();


};
