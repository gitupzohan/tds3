// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Structure/TPS_EnvironmentStructure.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATPS_EnvironmentStructure::ATPS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	CollisionSphere->SetSphereRadius(16.f);
}

// Called when the game starts or when spawned
void ATPS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTPS_StateEffect*> ATPS_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_EnvironmentStructure::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	ITPS_IGameActor::RemoveEffect(RemoveEffect);
}

void ATPS_EnvironmentStructure::AddEffect(UTPS_StateEffect* newEffect)
{
	ITPS_IGameActor::AddEffect(newEffect);
}

FVector ATPS_EnvironmentStructure::GetStateEffectLocBone(FName& BoneName)
{
	return ITPS_IGameActor::GetStateEffectLocBone(BoneName);
}



void ATPS_EnvironmentStructure::Explose()
{
	if (ExploseSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExploseSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ExploseSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExploseSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ExploseSetting.ExploseMaxDamage,
		ExploseSetting.ExploseMaxDamage * ExploseSetting.ExploseCoeffDeclineDamage,
		GetActorLocation(),
		ExploseSetting.ExploseRadiusMaxDamage,
		ExploseSetting.ExploseRadiusDeclineDamage,
		5,
		NULL, IgnoredActor, this, nullptr);
	if (ExploseSetting.DrawDebug)
		DrawDebugSphere(GetWorld(), CollisionSphere->GetComponentLocation(), ExploseSetting.ExploseRadiusMaxDamage, 16, FColor::Red, false, 5.0f);
		DrawDebugSphere(GetWorld(), CollisionSphere->GetComponentLocation(), ExploseSetting.ExploseRadiusDeclineDamage, 16, FColor::Blue, false, 5.0f);
	//this->Destroy();
}


