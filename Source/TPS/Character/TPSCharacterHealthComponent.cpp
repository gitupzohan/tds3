// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/TPSCharacterHealthComponent.h"

void UTPSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f && HaveShield)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

float UTPSCharacterHealthComponent::GetCurrentStamina()
{
	return Stamina;
}

void UTPSCharacterHealthComponent::ChangeStaminaValue(float ChangeValue)
{
	Stamina += ChangeValue;
	OnStaminaChange.Broadcast(Stamina, ChangeValue);
	if (Stamina > 100.0f)
	{
		Stamina = 100.0f;
	}
	else
	{
		if (Stamina < 0.0f)
			Stamina = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownStaminaTimer, this, &UTPSCharacterHealthComponent::CoolDownStaminaEnd, CoolDownStaminaRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
	}
}

void UTPSCharacterHealthComponent::CoolDownStaminaEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_StaminaRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryStamina, StaminaRecoverRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryStamina()
{
	float tmp = Stamina;
	tmp = tmp + StaminaRecoverValue;
	if (tmp > 100.0f)
	{
		Stamina = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
		}
	}
	else
	{
		Stamina = tmp;
	}

	OnStaminaChange.Broadcast(Stamina, StaminaRecoverValue);
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	OnShieldChange.Broadcast(Shield, ChangeValue);
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UTPSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}