// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Character/TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChange, float, Stamina, float, Change);

UCLASS()
class TPS_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnStaminaChange OnStaminaChange;

protected:

	float Shield = 100.0f;
	float Stamina = 100.0f;

public:

	//Health==========================================
	void ChangeHealthValue(float ChangeValue) override;

	//Shield=============================================
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		bool HaveShield = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UFUNCTION(BlueprintCallable, Category = "Shield")
		float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();
	
	//Stamina=============================================
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		bool HaveStamina = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float CoolDownStaminaRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverRate = 0.1f;

	FTimerHandle TimerHandle_CoolDownStaminaTimer;
	FTimerHandle TimerHandle_StaminaRecoveryRateTimer;

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		float GetCurrentStamina();
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void ChangeStaminaValue(float ChangeValue);
	void CoolDownStaminaEnd();
	void RecoveryStamina();
};
