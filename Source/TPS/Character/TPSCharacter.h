// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapon/WeaponDefault.h"
#include "TPSGameinstance.h"
#include "TPS/Character/TPSInventoryComponent.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/Weapon/Projectile/ProjectileDefault.h"
#include "TPSCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharDead);

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATPSCharacter();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "WeaponStatus")
		FOnReloadStart OnReloadStart;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "CharDead")
		FOnCharDead OnCharDead;

	FTimerHandle TimerHandle_RagDollTimer;
	FTimerHandle TimerHandle_StaminaStepTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:

	//Inputs
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION()
		void AimPressed();
	UFUNCTION()
		void AimReleased();
	UFUNCTION()
		void WalkPressed();
	UFUNCTION()
		void WalkReleased();
	UFUNCTION()
		void SprintPressed();
	UFUNCTION()
		void SprintReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;

		/*template<int32 Id>
		void TKeyPressed()
		{
			TrySwitchWeaponToIndexByKeyInput(Id);
		}*/

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool CrouchEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool RunEnabled = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintEnabled = false;
	UPROPERTY()//EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool moveBack = false;
	UPROPERTY()
		bool moveForward = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float StaminaIncrementValue = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float StaminaStepTime = 0.05f;

		void StaminaChange();

	//AnimDeads
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	//AbilityEffect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPS_StateEffect> AbilityEffect;

	//Camera
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraZoom")
		float ArmLengthMax = 1000.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraZoom")
		float ArmLengthMin = 650.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraZoom")
		float ScrollDistance = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraZoom")
		float StepTimer = 0.001f;
	UPROPERTY()
		bool SlideDone = true;
	UPROPERTY()
		int Count = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraZoom")
		float StepSmooth = 1.0f;
	UPROPERTY()
		float CurrentStepDistance = 0.0f;

	UPROPERTY()
		FTimerHandle FuzeTimerHandle;

	//Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	//Func
	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)//VisualOnly
		void RemoveCurrentWeapon();

	//Movement
	UFUNCTION()
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION()
		void MoveBack();
	UFUNCTION()
		void MoveForward();
	UFUNCTION()
		void LockSprintState();
	UFUNCTION()
		void SetMoveSpeedBack();
	UFUNCTION(BlueprintCallable)
		void ZoomCamera(float AxisWheel);
	UFUNCTION()
		void SlideCameraSmoothDown();
	UFUNCTION()
		void SlideCameraSmoothUp();

	//WeaponLogic
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart(UAnimMontage* Anim,float Time);
		void WeaponReloadStart_Implementation(UAnimMontage* Anim, float Time);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
		void WeaponReloadEnd_Implementation(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFiring(UAnimMontage* AnimHip, UAnimMontage* AnimAim);
		void WeaponFiring_Implementation(UAnimMontage* AnimHip, UAnimMontage* AnimAim);
	UFUNCTION()
		void WeaponReloadStartDelegate(UAnimMontage* Anim, float Time);

	//Inventory Func
		void DropCurrentWeapon();
		void TrySwicthFirstWeapon();
		void TrySwicthSecondWeapon();
		void TrySwicthThirdWeapon();
		void TrySwicthFourthWeapon();
		void TrySwicthFifthWeapon();
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;
		
	//Ability Func
	void TryAbilityEnabled();

	//Interface
		EPhysicalSurface GetSurfaceType() override;
		TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
		void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
		void AddEffect(UTPS_StateEffect* newEffect) override;

	UFUNCTION()
		void CharDead();
	UFUNCTION(BlueprintCallable)
		void EnableRagdoll();
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(BlueprintCallable)
		FVector GetStateEffectLocBone(FName& BoneName) override;
	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
		void CharDead_BP_Implementation();
};
		

