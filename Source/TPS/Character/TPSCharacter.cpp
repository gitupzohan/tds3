// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TPS/FuncLibrary/Types.h"
#include "Math/Vector.h"
#include "Engine/World.h"
#include "TPS/Game/TPSGameInstance.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = ArmLengthMax;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("HealthComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPSCharacter::InitWeapon);
	}
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATPSCharacter::CharDead);
	}

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
	LockSprintState();
	SetMoveSpeedBack();
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName, WeaponInfo,0);
	//if (InventoryComponent)
		//InventoryComponent->InitSlot();
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("Aiming"), EInputEvent::IE_Pressed, this, &ATPSCharacter::AimPressed);
	NewInputComponent->BindAction(TEXT("Aiming"), EInputEvent::IE_Released, this, &ATPSCharacter::AimReleased);

	NewInputComponent->BindAction(TEXT("Walk"), EInputEvent::IE_Pressed, this, &ATPSCharacter::WalkPressed);
	NewInputComponent->BindAction(TEXT("Walk"), EInputEvent::IE_Released, this, &ATPSCharacter::WalkReleased);

	NewInputComponent->BindAction(TEXT("Sprint"), EInputEvent::IE_Pressed, this, &ATPSCharacter::SprintPressed);
	NewInputComponent->BindAction(TEXT("Sprint"), EInputEvent::IE_Released, this, &ATPSCharacter::SprintReleased);

	NewInputComponent->BindAction(TEXT("SwitchWeapon1"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwicthFirstWeapon);
	NewInputComponent->BindAction(TEXT("SwitchWeapon2"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwicthSecondWeapon);
	NewInputComponent->BindAction(TEXT("SwitchWeapon3"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwicthThirdWeapon);
	NewInputComponent->BindAction(TEXT("SwitchWeapon4"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwicthFourthWeapon);
	NewInputComponent->BindAction(TEXT("SwitchWeapon5"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwicthFifthWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Released, this, &ATPSCharacter::TryAbilityEnabled);

	/*TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATPSCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATPSCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATPSCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATPSCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATPSCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATPSCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATPSCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATPSCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATPSCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATPSCharacter::TKeyPressed<0>);*/

	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::DropCurrentWeapon);
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::InputAttackPressed()
{
	if(bIsAlive && MovementState != EMovementState::Sprint_State)
		AttackCharEvent(true);
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::AimPressed()
{
	AimEnabled = true;
	SprintEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::AimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::WalkPressed()
{
	WalkEnabled = true;
	SprintEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::WalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::SprintPressed()
{
	if (!moveBack)
	{
		SprintEnabled = true;
		if (GetWorld())
		{
			GetWorldTimerManager().SetTimer(TimerHandle_StaminaStepTimer, this, &ATPSCharacter::StaminaChange, StaminaStepTime, true);
		}
		ChangeMovementState();
	}	
}

void ATPSCharacter::SprintReleased()
{
	SprintEnabled = false;
	if (GetWorld())
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_StaminaStepTimer);
	}
	ChangeMovementState();
}

void ATPSCharacter::StaminaChange()
{
	if (SprintEnabled)
	{
		if (HealthComponent)
		{
			HealthComponent->ChangeStaminaValue(StaminaIncrementValue);
			if (HealthComponent->GetCurrentStamina() <= 0.0f)
				SprintEnabled = false;
				ChangeMovementState();
		}
	}
	else
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_StaminaStepTimer);
	}
	
}

void ATPSCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
			float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::AimRun_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Sprint_State:
					break;
				default:
					break;
				}

				CurrentWeapon->ShotEndLocation = ResultHit.Location + Displacement;
			}
		}
		if (CurrentWeapon)
			if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
				CurrentWeapon->ShouldReduceDispersion = true;
			else
				CurrentWeapon->ShouldReduceDispersion = false;
	}
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATPSCharacter::CharacterUpdate()
{
	float ResSpeed = 600;
	switch (MovementState)
	{
	case EMovementState::AimCrouch_State:
		ResSpeed = MovementInfo.CrouchSpeed;
		break;
	case EMovementState::Crouch_State:
		ResSpeed = MovementInfo.CrouchSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::AimRun_State:
		ResSpeed = MovementInfo.AimRunSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	if (moveBack)
	{
		float Speed = ResSpeed * 0.8;
		GetCharacterMovement()->MaxWalkSpeed = Speed;
		Count = 0;
	}
}

void ATPSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			if(!CurrentWeapon->WeaponReloading)
				CurrentWeapon->InitReload();
	}
}

void ATPSCharacter::RemoveCurrentWeapon()
{
}

void ATPSCharacter::ChangeMovementState()
{
	if (!CrouchEnabled && !WalkEnabled && !AimEnabled && !SprintEnabled)
	{
		//RunEnabled = true;
		MovementState = EMovementState::Run_State;
	}
	else 
	{
		if (SprintEnabled && !WalkEnabled && !CrouchEnabled && !moveBack)
		{
			WalkEnabled = false;
			AimEnabled = false;
			/*AWeaponDefault* myWeapon = GetCurrentWeapon();
			if (myWeapon)
			{
				myWeapon->WeaponFiring = false;
			}*/
			MovementState = EMovementState::Sprint_State;
		}
		
		if (CrouchEnabled && !AimEnabled && !SprintEnabled)
		{
			if (MovementState != EMovementState::Sprint_State)
			{
				MovementState = EMovementState::Crouch_State;
			}
		}
		if (CrouchEnabled && AimEnabled && !SprintEnabled)
		{
			MovementState = EMovementState::AimCrouch_State;
		}
		
		if (WalkEnabled && AimEnabled && !SprintEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		if (WalkEnabled && !AimEnabled && !SprintEnabled)
		{
			MovementState = EMovementState::Walk_State;
		}
		if (!WalkEnabled && AimEnabled && RunEnabled && !CrouchEnabled )
		{
			MovementState = EMovementState::AimRun_State;
		}
		
	}
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)//ToDo Init by id row by table
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					myWeapon->WeaponSetting = myWeaponInfo;
					//Remove!!Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->UpdateStateWeapon(MovementState);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStartDelegate);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFiring.AddDynamic(this, &ATPSCharacter::WeaponFiring);
					myWeapon->WeaponInfo = WeaponAdditionalInfo;
					if(InventoryComponent)
						CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					//if (InventoryComponent)
						//InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType,true);
				}
			}
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
	}
	
}		

void ATPSCharacter::MoveBack()
{
	FVector Input = GetCharacterMovement()->GetLastInputVector();
	FVector BwdV = GetActorForwardVector() * -1.0f;
	moveBack = Input.Equals(BwdV, 1.0f);
}

void ATPSCharacter::MoveForward()
{
	FVector Input = GetCharacterMovement()->GetLastInputVector();
	FVector FwdV = GetActorForwardVector();
	moveForward = Input.Equals(FwdV, 0.35f);
}

void ATPSCharacter::LockSprintState()
{
	MoveForward();
	if (!moveForward && SprintEnabled && Count == 1)
	{
		SprintEnabled = false;
		ChangeMovementState();
		SetMoveSpeedBack();
		
	}
}

void ATPSCharacter::SetMoveSpeedBack()
{
	MoveBack();
	if (moveBack)
	{
		SprintEnabled = false;
		if (Count == 1)
		{
			float Speed = GetCharacterMovement()->MaxWalkSpeed * 0.8;
			GetCharacterMovement()->MaxWalkSpeed = Speed;
			Count = 0;
		}
	}
	if (!moveBack)
	{
		ChangeMovementState();
		Count = 1;
	}
}

void ATPSCharacter::ZoomCamera(float AxisWheel)
{	
	if (SlideDone)
	{
		if (AxisWheel > 0.0f)
		{
			SlideDone = false;
			GetWorldTimerManager().SetTimer(FuzeTimerHandle, this, &ATPSCharacter::SlideCameraSmoothDown, StepTimer, true);
		}
		if (AxisWheel < 0.0f)
		{
			SlideDone = false;
			GetWorldTimerManager().SetTimer(FuzeTimerHandle, this, &ATPSCharacter::SlideCameraSmoothUp, StepTimer, true);
		}
	}
}

void ATPSCharacter::SlideCameraSmoothDown()
{
	CurrentStepDistance = CurrentStepDistance + StepSmooth;
	float NewArmLength = CameraBoom->TargetArmLength - StepSmooth;
	if (NewArmLength < ArmLengthMin)
	{ 
		CameraBoom->TargetArmLength = ArmLengthMin;
	}
	else { CameraBoom->TargetArmLength = NewArmLength; }
	if (CurrentStepDistance >= ScrollDistance)
	{
		CurrentStepDistance = 0.0f;
		SlideDone = true;
		GetWorldTimerManager().ClearTimer(FuzeTimerHandle);
	}
}

void ATPSCharacter::SlideCameraSmoothUp()
{
	CurrentStepDistance = CurrentStepDistance + StepSmooth;
	
	float NewArmLength = CameraBoom->TargetArmLength + StepSmooth;
	if (NewArmLength > ArmLengthMax) 
	{
		CameraBoom->TargetArmLength = ArmLengthMax;
	}
	else { CameraBoom->TargetArmLength = NewArmLength; }
	if (CurrentStepDistance >= ScrollDistance)
	{
		CurrentStepDistance = 0.0f;
		SlideDone = true;
		GetWorldTimerManager().ClearTimer(FuzeTimerHandle);
	}
}


void ATPSCharacter::WeaponReloadStart_Implementation(UAnimMontage* Anim,float Time)
{

}


void ATPSCharacter::WeaponReloadEnd_Implementation(bool bIsSuccess, int32 AmmoSafe)
{
	
}

void ATPSCharacter::WeaponFiring_Implementation(UAnimMontage* AnimHip, UAnimMontage* AnimAim)
{
}

void ATPSCharacter::WeaponReloadStartDelegate(UAnimMontage* Anim, float Time)
{
	OnReloadStart.Broadcast();
}

void ATPSCharacter::DropCurrentWeapon()
{
 if(InventoryComponent)
 {
	 FDropItem ItemInfo;
	 if(CurrentIndexWeapon != 0)
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
		TrySwicthFirstWeapon();
 }
}

void ATPSCharacter::TrySwicthFirstWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int32 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if(OldIndex != 0)
			InventoryComponent->SwitchWeaponToIndex(0, OldIndex, OldInfo);	
		}
	}
}

void ATPSCharacter::TrySwicthSecondWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int32 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (OldIndex != 1)
				InventoryComponent->SwitchWeaponToIndex(1, OldIndex, OldInfo);
		}
	}
}

void ATPSCharacter::TrySwicthThirdWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int32 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (OldIndex != 2)
				InventoryComponent->SwitchWeaponToIndex(2, OldIndex, OldInfo);
		}
	}
}

void ATPSCharacter::TrySwicthFourthWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int32 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (OldIndex != 3)
				InventoryComponent->SwitchWeaponToIndex(3, OldIndex, OldInfo);
		}
	}
}

void ATPSCharacter::TrySwicthFifthWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int32 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (OldIndex != 4)
				InventoryComponent->SwitchWeaponToIndex(4, OldIndex, OldInfo);
		}
	}
}

void ATPSCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			FHitResult Hit;
			NewEffect->InitObject(this,Hit.BoneName);
		}
	}
}

EPhysicalSurface ATPSCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	
	return Result;
}

TArray<UTPS_StateEffect*> ATPSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSCharacter::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	ITPS_IGameActor::RemoveEffect(RemoveEffect);
	//Effects.Remove(RemoveEffect);
}

void ATPSCharacter::AddEffect(UTPS_StateEffect* newEffect)
{
	ITPS_IGameActor::AddEffect(newEffect);
	//Effects.Add(newEffect);
}

void ATPSCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	//if (GetController())
	//{
	//	GetController()->UnPossess();
	//}

	//Timer rag doll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATPSCharacter::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);
	AttackCharEvent(false);
	//DropCurrentWeapon();
	//CharDead_BP();
}

void ATPSCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
	if (GetController())
	{
		GetController()->UnPossess();
	}
	DropCurrentWeapon();
	CharDead_BP();
}

float ATPSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		if (HealthComponent)
			HealthComponent->ChangeHealthValue(-DamageAmount);

		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			FHitResult Hit;
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType(),Hit.BoneName);
		}
	}

	//AddHitEffectInterface================================
	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			FHitResult Hit;
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType(),Hit.BoneName);
		}
	}
	return ActualDamage;
}

FVector ATPSCharacter::GetStateEffectLocBone(FName& BoneName)
{
	return ITPS_IGameActor::GetStateEffectLocBone(BoneName);
}

void ATPSCharacter::CharDead_BP_Implementation()
{
}



