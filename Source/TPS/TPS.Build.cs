// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPS : ModuleRules
{
	public TPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });

        PublicIncludePaths.AddRange(new string[]
        {
            "TPS/Character",
            "TPS/FuncLibrary",
            "TPS/Game",
            "TPS/Structure",
            "TPS/StateEffect",
            "TPS/Interface",
            "TPS/Weapon",
	    "TPS/Weapon/Projectile"
        });
    }
}
