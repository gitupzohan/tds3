// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "DrawDebugHelpers.h"

#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
	
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
	if (ProjectileSetting.DrawDebug)
		DrawDebugSphere(GetWorld(), BulletCollisionSphere->GetComponentLocation(), ProjectileSetting.ExploseRadiusMaxDamage, 16, FColor::Red, false, TimerToExplose);
		DrawDebugSphere(GetWorld(), BulletCollisionSphere->GetComponentLocation(), ProjectileSetting.ExploseRadiusDeclineDamage, 16, FColor::Blue, false, TimerToExplose);
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * ProjectileSetting.ExploseCoeffDeclineDamage,
		GetActorLocation(),
		ProjectileSetting.ExploseRadiusMaxDamage,
		ProjectileSetting.ExploseRadiusDeclineDamage,
		5,
		NULL, IgnoredActor, this, nullptr);
	if (ProjectileSetting.DrawDebug)
		DrawDebugSphere(GetWorld(), BulletCollisionSphere->GetComponentLocation(), ProjectileSetting.ExploseRadiusMaxDamage, 16, FColor::Red, false, 5.0f);
		DrawDebugSphere(GetWorld(), BulletCollisionSphere->GetComponentLocation(), ProjectileSetting.ExploseRadiusDeclineDamage, 16, FColor::Blue, false, 5.0f);
	this->Destroy();
}
