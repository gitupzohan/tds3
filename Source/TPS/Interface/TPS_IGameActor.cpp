// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Interface/TPS_IGameActor.h"

// Add default functionality here for any ITPS_IGameActor functions that are not pure virtual.

EPhysicalSurface ITPS_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTPS_StateEffect*> ITPS_IGameActor::GetAllCurrentEffects()
{
	TArray<UTPS_StateEffect*> Effect;
	return Effect;
}

void ITPS_IGameActor::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ITPS_IGameActor::AddEffect(UTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}

FVector ITPS_IGameActor::GetStateEffectLocBone(FName& BoneName)
{
	FName Bone("None");
	BoneName = Bone;
	FVector EffectLocation(0);
	if (Effects.Num() > 0)
	{
		if (Effects[Effects.Num() - 1]->ParticleEmitter)
		{
			EffectLocation = Effects[Effects.Num() - 1]->ParticleEmitter->GetComponentLocation();
			Bone = Effects[Effects.Num() - 1]->ParticleEmitter->GetAttachSocketName();
			BoneName = Bone;
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::GetStateEffectLocBone"));
			return EffectLocation;
		}
	}
	return EffectLocation;
	//FName Bone;
	//FVector EffectLocation(0);
	//return FVector();
}

void ITPS_IGameActor::DropWeaponToWorld_Implementation(FDropItem DropItemInfo)
{
}

