// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TPSGameMode.generated.h"

UCLASS(minimalapi)
class ATPSGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATPSGameMode();

	void PlayerCharacterDead();
};



