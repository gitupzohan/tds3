// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSGameMode.h"
#include "TPSPlayerController.h"
#include "TPS/Character/TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPSGameMode::ATPSGameMode()
{
	// Use only with blueprint classes
	PlayerControllerClass = ATPSPlayerController::StaticClass();
	DefaultPawnClass = ATPSCharacter::StaticClass();
}

void ATPSGameMode::PlayerCharacterDead()
{
}
