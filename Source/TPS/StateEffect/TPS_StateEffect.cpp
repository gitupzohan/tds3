// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/StateEffect/TPS_StateEffect.h"
#include "TPS/Character/TPSHealthComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UTPS_StateEffect::InitObject(AActor* Actor, FName HitBone)
{
	UE_LOG(LogTemp, Warning, TEXT("UTPS_StateEffect::InitObject"));
	myActor = Actor;

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	
	return true;
}

void UTPS_StateEffect::DestroyObject()
{
	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName HitBone)
{
	Super::InitObject(Actor, HitBone);
	ExecuteOnce();
	return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName HitBone)
{
	Super::InitObject(Actor, HitBone);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		if (myActor)
		{
			FVector Loc = FVector(0);
			USkeletalMeshComponent* SkeletalMesh = Cast<USkeletalMeshComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			if (SkeletalMesh)
			{
				Loc = SkeletalMesh->GetBoneLocation(HitBone, EBoneSpaces::WorldSpace);
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, SkeletalMesh, HitBone, Loc, FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition, false);
				ParticleEmitter->SetRelativeScale3D((SkeletalMesh->GetBoneTransform(SkeletalMesh->GetBoneIndex(HitBone)).GetScale3D())/2);
				
			}
			else
			{
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), HitBone, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}	
		}
	}

	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

