// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS_ICombatEnemy.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPS_ICombatEnemy : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_API ITPS_ICombatEnemy
{
	GENERATED_BODY()

	

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		bool CanAttack();
		virtual bool CanAttack_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		bool CanMove();
		virtual bool CanMove_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void MeleeAttackLight();
		void MeleeAttackLight_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void MeleeAttackHeavy();
		void MeleeAttackHeavy_Implementation();
};
