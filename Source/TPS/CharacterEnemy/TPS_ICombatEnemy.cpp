// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/CharacterEnemy/TPS_ICombatEnemy.h"

// Add default functionality here for any ITPS_ICombatEnemy functions that are not pure virtual.

bool ITPS_ICombatEnemy::CanAttack_Implementation()
{
	return false;
}

bool ITPS_ICombatEnemy::CanMove_Implementation()
{
	return false;
}

void ITPS_ICombatEnemy::MeleeAttackLight_Implementation()
{
}

void ITPS_ICombatEnemy::MeleeAttackHeavy_Implementation()
{
}
