// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/CharacterEnemy/TPS_ICombatEnemy.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "CharacterEnemyBase.generated.h"

UCLASS()
class TPS_API ACharacterEnemyBase : public ACharacter, public ITPS_IGameActor, public ITPS_ICombatEnemy
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterEnemyBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actions")
		bool CanMovePawn = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actions")
		bool CanAttackPawn = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
		TArray<UAnimMontage*> DeadsAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
		TArray<UAnimMontage*> StunAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
		TArray<UAnimMontage*> LightAttackAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
		TArray<UAnimMontage*> HeavyAttackAnim;
	//Effect
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
		UAnimMontage* GetRandomAnimMontage(TArray<UAnimMontage*> ArrMontages);
	UFUNCTION(BlueprintCallable)
		FVector GetStateEffectLocBone(FName &BoneName) override;
	void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
	void AddEffect(UTPS_StateEffect* newEffect) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION(BlueprintCallable)
		bool CanAttack_Implementation() override;
	UFUNCTION(BlueprintCallable)
		bool CanMove_Implementation() override;

};
