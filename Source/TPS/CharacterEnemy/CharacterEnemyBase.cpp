// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/CharacterEnemy/CharacterEnemyBase.h"
#include "Animation/AnimMontage.h"

// Sets default values
ACharacterEnemyBase::ACharacterEnemyBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void ACharacterEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACharacterEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UAnimMontage* ACharacterEnemyBase::GetRandomAnimMontage(TArray<UAnimMontage*> ArrMontages)
{
	if (ArrMontages.Num() > 0)
	{
		for (int i = 0; i < ArrMontages.Num(); i++)
		{
			if (ArrMontages[i] == nullptr)
			{
				ArrMontages.RemoveAt(i);
				if (ArrMontages.Num() == 0)
					return nullptr;
				i = 0;
			}
		}
		//Clean array at empty ptr;
	}
	if (ArrMontages.Num() == 1)
	{
		if(IsValid(ArrMontages[0]))
			return ArrMontages[0];
		//if array contains one element, do not random montage;
	}
	else
	{
		int32 RandRange = FMath::RandRange(0, ArrMontages.Num() - 1);
		if (ArrMontages.IsValidIndex(RandRange))
		{
			return ArrMontages[RandRange];
		}
		return nullptr;
	}
	return nullptr;
}


FVector ACharacterEnemyBase::GetStateEffectLocBone(FName& BoneName)
{
	return ITPS_IGameActor::GetStateEffectLocBone(BoneName);
}

void ACharacterEnemyBase::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	ITPS_IGameActor::RemoveEffect(RemoveEffect);
}

void ACharacterEnemyBase::AddEffect(UTPS_StateEffect* newEffect)
{
	ITPS_IGameActor::AddEffect(newEffect);
}

// Called to bind functionality to input
void ACharacterEnemyBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ACharacterEnemyBase::CanAttack_Implementation()
{
	return CanAttackPawn;
}

bool ACharacterEnemyBase::CanMove_Implementation()
{
	return CanMovePawn;
}

